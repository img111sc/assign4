/* Chris Stevens-Edgar 
assignment 4
IMG111 - intro to Javascript
instructor: Frank Niscak
*/
//Create a function  italicParagraphs(id)  that will change
//font of all paragraphs in Part1 to italic. The function should be called italicParagraphs('part1'); .
  //create a variable to get part one
  //create a variable to get the paragraphs in part one 
  var getPart1 = document.getElementById("part1");
  var paragraphs = getPart1.getElementsByTagName("p");
  
  function italicParagraphs(id){
  for (var counter = 0; counter < id.length; counter++) {
   id [counter].style.fontStyle = 'italic';

  }
  }
  italicParagraphs(paragraphs);

  // change a button text color to red
function makeButtonTextRed(id) {
    var button = document.getElementById(id);
    
    if (!button)
    {
      alert('makeButtonTextRed - no button found');
      return;
    }
    
    button.style.color = " red ";
  }
  makeButtonTextRed('btn01');
  makeButtonTextRed('btn04');
  //makeButtonTextRed('btnx4'); 